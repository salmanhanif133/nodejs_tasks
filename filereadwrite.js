const fs = require('fs');
const http = require('http');


// fs.readFile('./sample.txt', 'utf-8', (err, data) => {
//   if (err) {throw err};

//   fs.writeFile('./message.txt', data, (err) => {
//     if (err) throw err;
//     console.log('The file has been saved!');
//   });
// });


const server = http.createServer((req, res) => {
  if (req.method === 'POST') {
    let body = '';
    req.on('data', chunk => {
        body = chunk.toString();
    });
    req.on('end', () => {
        console.log(body);
        res.end(body);
    });
}
  else {
    res.end(`
      <!doctype html>
      <html>
      <body>
          <form action="/" method="post">
              <input type="text" name="fname" /><br />
              <input type="number" name="age" /><br />
              <button>Save</button>
          </form>
      </body>
      </html>
    `);
  }
});
server.listen(8000);